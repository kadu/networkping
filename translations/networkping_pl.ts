<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>@default</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Zaawansowane</translation>
    </message>
    <message>
        <source>Network ping</source>
        <translation>Pingowanie sieci</translation>
    </message>
    <message>
        <source>Check the network availability every</source>
        <translation>Sprawdzaj dostępność sieci co</translation>
    </message>
    <message numerus="yes">
        <source>[every] %n second(s)</source>
        <translation>
            <numerusform>[co] %n sekundę</numerusform>
            <numerusform>[co] %n sekundy</numerusform>
            <numerusform>[co] %n sekund</numerusform>
        </translation>
    </message>
    <message>
        <source>IP address of host to ping</source>
        <translation>Adres IP pingowanego hosta</translation>
    </message>
    <message>
        <source>Number of port to ping on</source>
        <translation>Numer portu do pingowania</translation>
    </message>
    <message>
        <source>Timeout after</source>
        <translation>Przedawnienie połączenia po</translation>
    </message>
    <message numerus="yes">
        <source>[after] %n second(s)</source>
        <translation>
            <numerusform>[po] %n sekundzie</numerusform>
            <numerusform>[po] %n sekundach</numerusform>
            <numerusform>[po] %n sekundach</numerusform>
        </translation>
    </message>
    <message>
        <source>Detect external IP address changes</source>
        <translation>Wykrywaj zmiany zewnętrznego adresu IP</translation>
    </message>
    <message>
        <source>External IP address checker</source>
        <translation>Usługa sprawdzania zewnętrznego adresu IP</translation>
    </message>
    <message>
        <source>Check external IP address every</source>
        <translation>Sprawdzaj zewnętrzny adres IP co</translation>
    </message>
    <message>
        <source>Timeout checking after</source>
        <translation>Przedawnienie sprawdzania po</translation>
    </message>
</context>
</TS>
