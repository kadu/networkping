<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source>Network ping</source>
        <translation>Network ping</translation>
    </message>
    <message>
        <source>IP address of host to ping</source>
        <translation>IP address of host to ping</translation>
    </message>
    <message>
        <source>Number of port to ping on</source>
        <translation>Number of port to ping on</translation>
    </message>
    <message>
        <source>Timeout after</source>
        <translation>Timeout after</translation>
    </message>
    <message>
        <source>Detect external IP address changes</source>
        <translation>Detect external IP address changes</translation>
    </message>
    <message>
        <source>External IP address checker</source>
        <translation>External IP address checker</translation>
    </message>
    <message numerus="yes">
        <source>[every] %n second(s)</source>
        <translation>
            <numerusform>[every] %n second</numerusform>
            <numerusform>[every] %n seconds</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>[after] %n second(s)</source>
        <translation>
            <numerusform>[after] %n second</numerusform>
            <numerusform>[after] %n seconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Check the network availability every</source>
        <translation>Check the network availability every</translation>
    </message>
    <message>
        <source>Check external IP address every</source>
        <translation>Check external IP address every</translation>
    </message>
    <message>
        <source>Timeout checking after</source>
        <translation>Timeout checking after</translation>
    </message>
</context>
</TS>
